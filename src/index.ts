interface PriceParameters {
    price: number
    discount: number
    isInstallment: boolean
    months: number
}

const totalPrice = ({ price, discount, isInstallment, months }: PriceParameters): number => {
    const fullPrice: number = price - discount / 100 * price

    if (isInstallment && months > 0) {
        return fullPrice / months
    }
    return fullPrice
}

const totalPriceCalculated: number = totalPrice({ price: 100000, discount: 25, isInstallment: true, months: 12 })
console.log(totalPriceCalculated)
// 6250